% -*- matlab -*-
% generate duality.dat

rand('seed',42);
A = rand(4,3) - 0.5;
npt = 200;
pmin = 1.7;
pmax = 1/(1-1/pmin);

fd = fopen('evals.dat','w');

for i = 0:npt

  p = pmin + (pmax-pmin)*i/(npt);
  pc = 1/(1-1/p);
  
  [N0,v0,n0] = opnorm(A,p,p);
  [N1,v1,n1] = opnorm(A',pc,pc);
  
  fprintf(fd,'%f %8i %8i\n',p,n0,n1)
  fprintf('%f %f %f\n',p,N0,N1)
  
end

fclose(fd);