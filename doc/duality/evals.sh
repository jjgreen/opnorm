#!/bin/sh
# GMT script for duality plot

DAT="evals.dat"
EPS="evals.eps"
RNG="-R1.7/2.429/1e2/5e5"
PRJ="-JX4i/3il"
DEF="+defaults"
COM="$DEF $PRJ $RNG"

cat $DAT | awk '{ print $1,$2 }' | \
    GMT psxy $COM -Wthin,black -K > $EPS

cat $DAT | awk '{ print $1,$3 }' | \
    GMT psxy $COM -Wthin,red -K -O >> $EPS

GMT psbasemap -Ba0.1f0.1/a2f3nSeW $COM -K -O >> $EPS

bboxx -insert $EPS > /dev/null
epstopdf $EPS
