/*
  patch.h

  projections of cubes onto the lp sphere

  Copyright (c) J.J. Green 2012
*/

#include "patch.h"
#include "pnorm.h"

void cube_to_patch(const cube_t *cube,
                   index_t n,
                   double p,
                   double LCRP,
                   double SCD,
                   patch_t *patch)
{
  double
    * restrict pcent = patch->centres,
    * restrict ccent = cube->centres,
    t = pnorm(ccent, n, p),
    rt = 1/t,
    halfwidth = cube_halfwidth(cube),
    D = SCD * halfwidth;

  for (index_t i = 0 ; i < n ; i++)
    pcent[i] = ccent[i] * rt;

  /* see ../doc/prescale.pdf */

  if (t > 1 + D)
    halfwidth /= (t-D);

  patch->radius = halfwidth * SCD * LCRP;
}
