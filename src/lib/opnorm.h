/*
  Calculate the operator norm of a matrix using the
  algorithm of S.W. Drury.

  J.J. Green, 2011
*/

#ifndef OPNORM_H
#define OPNORM_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>

/*
  calculate the operator norm of the mxn matrix M
  (represented as an array of double) with the
  specified majority (row/column major) considered
  as an operator from R^m (equipped with the p-norm,
  1 < p < infinity) to R^n (with the q-norm,
  1 <= q <= infinity) to a precision eps.

  On success, zero is returned and the norm, the (a)
  maximising n-vector and various statistics are assigned
  to the final arguments (if non NULL).

  On error, a positive value is returned. An appropriate
  error message can be found by passing the value to the
  opnorm_strerror() function
*/

enum majority_e {row_major, column_major};
typedef enum majority_e majority_t;

typedef struct
{
  unsigned long neval, nfifo, fifomax;
  unsigned nthread;
} opnorm_stats_t;

typedef struct
{
  double eps;
  size_t fifomax;
} opnorm_opt_t;

int opnorm(const double*, majority_t,
           size_t, size_t,
           double, double,
           opnorm_opt_t,
           double*, double*,
           opnorm_stats_t*);

const char* opnorm_strerror(int);

#ifdef __cplusplus
}
#endif

#endif
