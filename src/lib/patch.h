/*
  patch.h

  projections of cubes onto the lp sphere

  Copyright (c) J.J. Green 2012
*/

#ifndef PATCH_H
#define PATCH_H

#include "index.h"
#include "cube.h"

typedef struct
{
  double radius;
  double *centres;
} patch_t;

void cube_to_patch(const cube_t *cube,
                   index_t n,
                   double p,
                   double LCRP,
                   double SCD,
                   patch_t *patch);

#endif
