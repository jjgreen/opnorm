import numpy as np

base   = "i7-4700MQ"
ntexp  = 3
ordmin = 2
ordmax = 7

# read data

nt = 2**ntexp

T = np.zeros([ordmax-ordmin+1,nt])

for n in range(1, nt+1) :
    datfile = "{0!s}/opnorm-timing-t{1:d}.dat".format(base, n)
    D = np.loadtxt(datfile)
    T[:,n-1] = D[:,3]

# convert to millisec

T = T*1000

print "<thead>"

print "<tr>"
print "<th></th>"
print "<th colspan='{0:d}'>threads</th>".format((nt))
print "</tr>"

print "<tr>"
print "<th>order</th>"
for n in range(0, ntexp+1) :
    print "<th>{0:d}</th>".format((2**n))
print "</tr>"

print "</thead>"

print "<tbody>"

for m in range(ordmin, ordmax+1) :
    print "<tr>"
    print "<td>{0:d}</td>".format((m))
    for n in range(0, ntexp+1) :
        print "<td>{0:.3f}</th>".format((T[m-ordmin, 2**n-1]))
    print "</tr>"

print "</tbody>"
