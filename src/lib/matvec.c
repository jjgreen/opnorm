/*
  matvec.h

  matrix-vector multiply

  Copyright (c) J.J. Green 2012, 2019
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "matvec.h"

#ifdef WITH_BLAS
#include <cblas.h>
#endif

/*
  matrix-vector multiply, depending on the majority
  of the matrix layout
*/

void matvec_row_major(const double * restrict M,
                      const double * restrict x,
                      index_t m, index_t n,
                      double *y)
{

#ifdef WITH_BLAS

  cblas_dgemv(CblasRowMajor, CblasNoTrans, (const int)m, (const int)n,
	      1.0, M, n, x, 1, 0.0, y, 1);

#else

  for (index_t i = 0 ; i < m ; i++)
    {
      double sum = 0.0;

      for (index_t j = 0 ; j < n ; j++)
	sum += M[i*n + j] * x[j];

      y[i] = sum;
    }

#endif
}

void matvec_column_major(const double * restrict M,
                         const double * restrict x,
                         index_t m, index_t n,
                         double *y)
{

#ifdef WITH_BLAS

  cblas_dgemv(CblasColMajor, CblasNoTrans, (const int)m, (const int)n,
	      1.0, M, m, x, 1, 0.0, y, 1);

#else

  for (index_t i = 0 ; i < m ; i++)
    {
      double sum = 0.0;

      for (index_t j = 0 ; j < n ; j++)
	sum += M[i + j*m] * x[j];

      y[i] = sum;
    }

#endif
}
