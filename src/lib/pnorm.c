/*
  pnorm.c

  vector p-norms

  Copyright (c) J.J. Green 2012
*/

#include <math.h>

#include "pnorm.h"

/*
  the p-norm of the length n array v, 1 <= p < infinity,
  using the Kahane summation method, which is slightly
  more expensive -- but worth it
*/

static double pnorm_finite(const double *v, index_t n, double p)
{
  double sum = 0.0, c = 0.0;

  for (index_t i = 0 ; i < n ; i++)
    {
      double
	y = pow(fabs(v[i]), p) - c,
	t = sum + y;

      c = (t - sum) - y;
      sum = t;
    }

  return pow(sum, 1/p);
}

static double pnorm_infinite(const double *v, index_t n)
{
  double vmax = 0;

  for (index_t i = 0 ; i < n ; i++)
    {
      double vi = fabs(v[i]);
      if (vi > vmax) vmax = vi;
    }

  return vmax;
}

double pnorm(const double *v, index_t n, double p)
{
  return
    isinf(p) == 1 ?
    pnorm_infinite(v, n) :
    pnorm_finite(v, n, p);
}
