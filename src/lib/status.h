/*
  status.h

  return status for opnorm

  Copyright (c) J.J. Green 2012
*/

#ifndef STATUS_H
#define STATUS_H

/* possible return values for the main function */

#define OPNORM_OK        0
#define OPNORM_EDOM_P    1
#define OPNORM_EDOM_Q    2
#define OPNORM_EDOM_EPS  3
#define OPNORM_INACC     4
#define OPNORM_FIFO      5
#define OPNORM_ALLOC     6
#define OPNORM_USER      7
#define OPNORM_THREAD    8
#define OPNORM_BUG       9
#define OPNORM_FIFOMAX  10

const char* status_string(int err);

#endif
