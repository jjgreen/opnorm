/*
  cube.c

  cubes on the boundary of the l-infinity ball

  Copyright (c) J.J. Green 2012
*/

#include <math.h>
#include "cube.h"

double cube_halfwidth(const cube_t *cube)
{
  return ldexp(1, -(cube->hwnexp));
}
