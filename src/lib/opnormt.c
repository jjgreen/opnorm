/*
  Calculate the operator norm of a matrix using the
  algorithm of S.W. Drury [1].

  This is the POSIX thread version of the algorithm.

  J.J. Green, 2011, 2012, 2015
*/

/*
  References

  [1] S.W. Drury, "A counterexample to a conjecture of Matsaev",
  Lin. Alg. Appl., 435 (2011) 323-329.
*/

#define _POSIX_C_SOURCE 199506L

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define MAX_THREADS 1024

#include "opnorm.h"
#include "fifo.h"
#include "lcrp.h"
#include "index.h"
#include "pnorm.h"
#include "matvec.h"
#include "patch.h"
#include "cubefifo.h"
#include "status.h"
#include "trace.h"

#include <errno.h>
#include <string.h>
#include <math.h>

#include <float.h>

#include <pthread.h>
#include <unistd.h>

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

/* these are Lemmas 5 and 3 of [1], respectively */

static inline double radius_to_ratio(double rad, double p)
{
  return 1 - (p < 2 ?
	      pow(rad, p) :
	      0.5 * (p-1) * rad * rad);
}

/* thread data */

typedef struct
{
  index_t n, m;
  double p, q, eps, LCRP, SCD;
  const double *M;
  void (*matvec)(const double*, const double*, index_t, index_t, double*);
  fifo_t *fifo;
  double tmax, *vmax;
  pthread_mutex_t fifolock, maxlock;
} tdat_shared_t;

typedef struct
{
  index_t neval, nfifo;
  int status;
  tdat_shared_t *shared;
} tdat_t;

#ifdef HAVE_SIGNAL_H

static int set_cancellable(void)
{
  int dummy;

  if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &dummy) != 0)
    return 1;

  if (pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &dummy) != 0)
    return 1;

  return 0;
}

#endif

static void* worker(tdat_t *td)
{

#ifdef HAVE_SIGNAL_H

  /* set this thread as cancellable */

  if (set_cancellable() != 0)
    {
      td->status = OPNORM_THREAD;
      return NULL;
    }

#endif

  /*
    unpack arguments, mostly for readability but also saves
    a few dereferences
  */

  tdat_shared_t *ts = td->shared;

  index_t
    n = ts->n,
    m = ts->m;

  double
    p    = ts->p,
    q    = ts->q,
    eps  = ts->eps,
    LCRP = ts->LCRP,
    SCD  = ts->SCD;

  const double *M = ts->M;

  fifo_t *fifo = ts->fifo;

  /* working data */

  double  pcent[n];
  cube_t  cube0, cube1;
  patch_t patch;
  double  tmax = 0.0;

  patch.centres = pcent;

  while (1)
    {
      /* thread cancellation point  */

      pthread_testcancel();

      /* dequeue a cube */

      if (pthread_mutex_lock(&(ts->fifolock)) < 0)
	{
	  td->status = OPNORM_THREAD;
	  return NULL;
	}

      int fifoerr = fifo_dequeue(fifo, &cube0);

      if (pthread_mutex_unlock(&(ts->fifolock)) < 0)
	{
	  td->status = OPNORM_THREAD;
	  return NULL;
	}

      if (fifoerr != FIFO_OK)
	{
	  td->status = (fifoerr == FIFO_EMPTY ?
			OPNORM_OK :
			OPNORM_FIFO);
	  return NULL;
	}

      cube_print(&cube0, n, ">");

      /* nfifo is the total number dequeue */

      td->nfifo++;

      /* cube subdivide */

      int hwnexp = cube0.hwnexp + 1;
      double halfwidth = ldexp(1, -hwnexp);

      /*
	if halfwidth < DBL_EPSILON then we cannot
	calulate the centres of the subdivided cubes
	accurately, we break out and report that the
	requested accuracy could not be achieved
      */

      if (halfwidth < DBL_EPSILON)
	{
	  td->status = OPNORM_INACC;
	  return NULL;
	}

      for (size_t k = 0 ; k < (1UL << (n-1)) ; k++)
	{
	  cube1.side = cube0.side;
	  cube1.hwnexp = hwnexp;

	  /*
	    we give our cube1 a temporary set of centres
	    while we evaluate and decide whether to jetison
	    or enqueue it, only if the latter do we make a
	    malloc and copy the temporary centres.  this
	    saves a *lot* of malloc/free pairs
	  */

	  double centres[n];
	  cube1.centres = centres;

	  size_t k0 = k;

	  for (size_t j = 0 ; j < n ; j++)
	    {
	      if (cube0.side == j)
		{
		  cube1.centres[j] = cube0.centres[j];
		  continue;
		}

	      cube1.centres[j] = cube0.centres[j] +
		((k0 % 2) ? halfwidth : -halfwidth);

	      k0 /= 2;
	    }

	  cube_print(&cube1, n, "<");

	  /* get the corresponding patch */

	  cube_to_patch(&cube1, n, p, LCRP, SCD, &patch);
	  patch_print(patch, n);

	  double ratio = radius_to_ratio(patch.radius, p);

	  /*
	    check for patch viability - this check almost
	    always succeeds (on Drury K, this is false in
	    80/164016 cases, so 0.05% of the time) very
	    small beer. Yet it introduces a branch point,
	    so one might think it worth removing. Timing
	    tests indicate that there is no speedup in
	    doing so, so we keep it.
	  */

	  if (ratio > 0)
	    {
	      /* evaluate M at patch centre */

	      double v[m];

	      ts->matvec(M, pcent, m, n, v);
	      td->neval++;

	      double t = pnorm(v, m, q);

	      /* test first with the previous value of tmax */

	      if (t < tmax)
		{
		  /* test whether we can jettison this cube */

		  if (t < (tmax * ratio * (1 + eps))) continue;

		  /*
		    note that the global ts->tmax >= tmax so it would
		    be pointless (and cost a mutex access) to test
		    for that here
		  */
		}
	      else
		{
		  if (pthread_mutex_lock(&(ts->maxlock)) < 0)
		    {
		      td->status = OPNORM_THREAD;
		      return NULL;
		    }

		  /* update local tmax from global */

		  tmax = ts->tmax;

		  /*
		    if we have found a new global maximum then we
		    update it (and the global maximising vector)
		    as well as the local copy
		  */

		  if (t > tmax)
		    {
		      ts->tmax = tmax = t;

		      if (ts->vmax)
			memcpy(ts->vmax, pcent, n * sizeof(double));
		    }

		  if (pthread_mutex_unlock(&(ts->maxlock)) < 0)
		    {
		      td->status = OPNORM_THREAD;
		      return NULL;
		    }

		  /*
		    test whether we can jettison this cube but
		    now with the updated value of tmax
		  */

		  if (t < (tmax * ratio * (1 + eps)))
                    continue;

		}
	    }

	  /*
	    we will enqueue this cube, so we need to
	    allocate and copy its temporary centres set
	  */

	  if (! (cube1.centres = malloc(n*sizeof(double))))
	    {
	      td->status = OPNORM_ALLOC;
	      return NULL;
	    }

	  memcpy(cube1.centres, centres, n*sizeof(double));

	  if (pthread_mutex_lock(&(ts->fifolock)) < 0)
	    {
	      free(cube1.centres);
	      td->status = OPNORM_THREAD;
	      return NULL;
	    }

	  fifoerr = fifo_enqueue(fifo, &cube1);

	  if (pthread_mutex_unlock(&(ts->fifolock)) < 0)
	    {
	      td->status = OPNORM_THREAD;
	      return NULL;
	    }

	  switch(fifoerr)
	    {
	    case FIFO_OK:
	      break;
	    case FIFO_USERMAX:
	      td->status = OPNORM_FIFOMAX;
	      return NULL;
	    default:
	      td->status = OPNORM_FIFO;
	      return NULL;
	    }
	}

      free(cube0.centres);
    }

  /* we should not arrive here */

  td->status = OPNORM_BUG;

  return NULL;
}

#ifdef HAVE_SIGNAL_H

/*
  the signal handling thread which waits on a signal
  set sigset, and if it receives a signal then sends
  a pthread_cancel to all of the worker threads.

  this thread is itself cancelled by the main thread
  after all of the worker threads have joined, it
  is important that sigwait() is a cancellable function
  according the to POSIX standard
*/

typedef struct
{
  int n;
  pthread_t *thread;
  sigset_t *sigset;
} stdat_t;

static void* sigthread(stdat_t *stdat)
{
  int sig, err = sigwait(stdat->sigset, &sig);

  if (err != 0)
    return NULL;

  for (int i = 0 ; i < stdat->n ; i++)
    {
      err = pthread_cancel(stdat->thread[i]);
      if (err < 0)
	return NULL;
    }

  return NULL;
}

#endif

/* the main function */

int opnorm(const double *M, majority_t maj,
           size_t m, size_t n,
           double p, double q,
           opnorm_opt_t opt,
           double *norm,
           double *vmax,
           opnorm_stats_t *pstats)
{
  if ( (p <= 1.0) || (isinf(p) == 1) )
    {
      errno = EDOM;
      return OPNORM_EDOM_P;
    }

  if ( q < 1.0 )
    {
      errno = EDOM;
      return OPNORM_EDOM_Q;
    }

  if ( opt.eps <= 0.0 )
    {
      errno = EDOM;
      return OPNORM_EDOM_EPS;
    }

  if ( opt.eps <= DBL_EPSILON )
    {
      return OPNORM_INACC;
    }

  /* prepare shared thread data */

  tdat_shared_t ts =
    {
      .n = n,
      .m = m,
      .p = p,
      .q = q,
      .M = M,
      .eps = opt.eps,
      .tmax = 0,
      .vmax = vmax
    };

  /*
    mutexes - we maintain seperate mutexes for the
    fifo (queue and dequeue) and for the tmax variable,
    since the latter has a much higher access cadence
  */

  if (pthread_mutex_init(&(ts.fifolock), NULL) < 0)
    return OPNORM_FIFO;

  if (pthread_mutex_init(&(ts.maxlock), NULL) < 0)
    return OPNORM_FIFO;

  /*
    choose matrix-vector multiply function depending
    on the specified matrix majority (matvec.c)
  */

  switch (maj)
    {
    case row_major:
      ts.matvec = matvec_row_major;
      break;
    case column_major:
      ts.matvec = matvec_column_major;
      break;
    default:
      return OPNORM_BUG;
    }

  /* initialise the cube queue */

  if ( !(ts.fifo = init_fifo(n, opt.fifomax)) )
    return OPNORM_FIFO;

  /* Lipschitz constant for the radial projection */

  if (lcrp(p, &(ts.LCRP)) != 0)
    return OPNORM_BUG;

  /*
    surface cube diameter - the diameter of a (n-1)-cube
    of side one measured with the lp norm
  */

  ts.SCD = pow(n - 1, 1 / p);

  /* get number of threads */

  int nt = 1;

#if (defined HAVE_SYSCONF) && (defined _SC_NPROCESSORS_ONLN)

  nt = sysconf(_SC_NPROCESSORS_ONLN);

#endif

  char *ntenv = getenv("OPNORM_NTHREAD");

  if (ntenv)
    nt = atoi(ntenv);

  if (nt < 1)
    return OPNORM_THREAD;

  if (nt > MAX_THREADS)
    nt = MAX_THREADS;

  /*
    set up per-thread data - the status is set to
    OPNORM_USER since this field is assigned (to
    something else) before the worker threads exit
    normally. Having this as the default value means
    that those threads don't need to assign this
    field in response to a pthread_cancel() (which
    would require adding cleanup functions)
  */

  tdat_t td[nt];

  for (int i = 0 ; i < nt ; i++)
    {
      td[i].nfifo  = 0;
      td[i].neval  = 0;
      td[i].status = OPNORM_USER;
      td[i].shared = &ts;
    }

#ifdef HAVE_SIGNAL_H

  /* setup the signal handling thread */

  sigset_t sigset;

  sigemptyset(&sigset);
  sigaddset(&sigset, SIGQUIT);
  sigaddset(&sigset, SIGINT);
  sigaddset(&sigset, SIGUSR1);

  if (pthread_sigmask(SIG_BLOCK, &sigset, NULL) < 0)
    return OPNORM_THREAD;

#endif

  /* run worker threads */

  pthread_t thread[nt];

  for (int i = 0 ; i < nt ; i++)
    {
      if (pthread_create(thread + i,
			 NULL,
			 (void* (*)(void*))worker,
			 td + i) < 0)
	return OPNORM_THREAD;
    }

#ifdef HAVE_SIGNAL_H

  /* run signal handling thread */

  stdat_t stdat =
    {
      .n = nt,
      .thread = thread,
      .sigset = &sigset
    };

  pthread_t sigth;

  if (pthread_create(&sigth,
		     NULL,
		     (void* (*)(void*)) sigthread,
		     (void*) &stdat) < 0)
    return OPNORM_THREAD;

#endif

  /* join worker threads */

  for (int i = 0 ; i < nt ; i++)
    {
      if (pthread_join(thread[i], NULL) < 0)
	return OPNORM_THREAD;
    }

#ifdef HAVE_SIGNAL_H

  /* cancel and join the signal thread */

  if (pthread_cancel(sigth) < 0)
    return OPNORM_THREAD;

  if (pthread_join(sigth, NULL) < 0)
    return OPNORM_THREAD;

#endif

  /* harvest statistics */

  opnorm_stats_t stats =
    {
      .neval   = 0,
      .nfifo   = 0,
      .fifomax = 0,
      .nthread = nt
    };

  for (int i = 0 ; i < nt ; i++)
    {
      stats.nfifo += td[i].nfifo;
      stats.neval += td[i].neval;
    }

  stats.fifomax = fifo_peak(ts.fifo);

  /* clean up fifo */

  if (empty_fifo(ts.fifo) != 0)
    return OPNORM_FIFO;

  fifo_destroy(ts.fifo);

  /* clean up mutexes */

  if (pthread_mutex_destroy(&(ts.fifolock)) < 0)
    return OPNORM_THREAD;

  if (pthread_mutex_destroy(&(ts.maxlock)) < 0)
    return OPNORM_THREAD;

  /*
    harvest return values - the first that we find that
    is not OPNORM_OK we return
  */

  for (int i = 0 ; i < nt ; i++)
    {
      if ( td[i].status != OPNORM_OK )
	return td[i].status;
    }

  /* assign results */

  if (norm) *norm = ts.tmax;
  if (pstats) *pstats = stats;

  return OPNORM_OK;
}

/* error messages for opnorm() return values */

const char* opnorm_strerror(int err)
{
  return status_string(err);
}
