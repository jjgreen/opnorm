/*
  Matrix market header I/O library
  J.J. Green 2011
*/

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "mmhio.h"

#define MMH_BANNER    "%%MatrixMarket"

#define MMH_MTX_STR   "matrix"

#define MMH_ARRAY_STR "array"
#define MMH_COORD_STR "coordinate"

#define MMH_COMP_STR  "complex"
#define MMH_REAL_STR  "real"
#define MMH_PAT_STR   "pattern"
#define MMH_INT_STR   "integer"

#define MMH_GEN_STR   "general"
#define MMH_SYMM_STR  "symmetric"
#define MMH_HERM_STR  "hermitian"
#define MMH_SKEW_STR  "skew-symmetric"

#define LLEN 1025

int mmh_is_valid(mmh_code_t code)
{
  if ((code.coord == mmh_array) && (code.data == mmh_pattern))
    return 0;

  if ((code.data == mmh_real) && (code.storage == mmh_hermitian))
    return 0;

  if (
      (code.data == mmh_pattern) &&
      ((code.storage == mmh_hermitian) || (code.storage == mmh_skew))
      )
    return 0;

  return 1;
}

/* convert null delimited string to lowercase */

static void lowercase(char *st)
{
  char *p;

  for (p=st ; *p ; p++) *p = tolower(*p);
}

#define DELIM " \t\n"

int mmh_read_banner(FILE *f, mmh_code_t *code)
{
  char line[LLEN];

  if (fgets(line, LLEN, f) == NULL) return MMH_EOF;

  char *banner;

  if ((banner = strtok(line,DELIM)) == NULL) return MMH_EOF;

  char *t[4];

  for (int i = 0 ; i < 4 ; i++)
    {
      if ( (t[i] = strtok(NULL,DELIM)) == NULL )
	return MMH_EOF;
      lowercase(t[i]);
    }

  char
    *mtx     = t[0],
    *coord   = t[1],
    *data    = t[2],
    *storage = t[3];

  /* check for banner */

  if (strcmp(banner, MMH_BANNER) != 0)
    return MMH_NO_HEADER;

  /* first field should be "mtx" */

  if (strcmp(mtx, MMH_MTX_STR) != 0)
    return  MMH_BAD_OBJECT_TYPE;

  /*
    second field describes whether this is a sparse
    matrix (in coordinate storage) or a dense array
  */

  if (strcmp(coord, MMH_COORD_STR) == 0)
    code->coord = mmh_coordinate;
  else if (strcmp(coord, MMH_ARRAY_STR) == 0)
    code->coord = mmh_array;
  else
    return MMH_BAD_COORD_TYPE;

  /* third field */

  if (strcmp(data, MMH_REAL_STR) == 0)
    code->data = mmh_real;
  else if (strcmp(data, MMH_COMP_STR) == 0)
    code->data = mmh_complex;
  else if (strcmp(data, MMH_PAT_STR) == 0)
    code->data = mmh_pattern;
  else if (strcmp(data, MMH_INT_STR) == 0)
    code->data = mmh_integer;
  else
    return MMH_BAD_DATA_TYPE;

  /* fourth field */

  if (strcmp(storage, MMH_GEN_STR) == 0)
    code->storage = mmh_general;
  else if (strcmp(storage, MMH_SYMM_STR) == 0)
    code->storage = mmh_symmetric;
  else if (strcmp(storage, MMH_HERM_STR) == 0)
    code->storage = mmh_hermitian;
  else if (strcmp(storage, MMH_SKEW_STR) == 0)
    code->storage = mmh_skew;
  else
    return MMH_BAD_STORAGE_TYPE;

  return 0;
}

int mmh_write_mtx_crd_size(FILE *f, size_t m, size_t n, size_t nnz)
{
  if (fprintf(f, "%zi %zi %zi\n", m, n, nnz) < 0)
    return MMH_FWRITE;
  else
    return 0;
}

static int line_comment(const char* line)
{
  return line[0] == '%';
}

static int line_blank(const char* line)
{
  const char *p;

  for (p=line ; *p ; p++)
    switch (*p)
      {
      case ' ':
      case '\n':
      case '\t':
	break;
      default:
	return 0;
      }

  return 1;
}

static int skip_comments_blanks(FILE *f, size_t len, char *line)
{
  /* read line */

  if (fgets(line,len,f) == NULL)
    return 1;

  /* skip comments */

  while (line_comment(line))
    {
      if (fgets(line, len, f) == NULL)
	return 1;
    }

  /* skip empty lines */

  while (line_blank(line))
    {
      if (fgets(line, len, f) == NULL)
	return 1;
    }

  return 0;
}

int mmh_read_mtx_crd_size(FILE *f, size_t *m, size_t *n, size_t *nnz)
{
  char line[LLEN];

  if (skip_comments_blanks(f, LLEN, line) != 0)
    return MMH_EOF;

  return
    (sscanf(line, "%zi %zi %zi", m, n, nnz) == 3) ? 0 : MMH_EOF;
}

int mmh_read_mtx_array_size(FILE *f, size_t *m, size_t *n)
{
  char line[LLEN];

  if (skip_comments_blanks(f, LLEN, line) != 0)
    return MMH_EOF;

  return
    (sscanf(line, "%zi %zi", m, n) == 2) ? 0 : MMH_EOF;
}

int mmh_write_mtx_array_size(FILE *f, size_t m, size_t n)
{
  if (fprintf(f, "%zi %zi\n", m, n) < 0)
    return MMH_FWRITE;
  else
    return 0;
}

int mmh_write_banner(FILE *f, mmh_code_t code)
{
  const char *banner, *mtx, *coord, *data, *storage;

  banner = MMH_BANNER;
  mtx    = MMH_MTX_STR;

  switch (code.coord)
    {
    case mmh_coordinate : coord = MMH_COORD_STR; break;
    case mmh_array      : coord = MMH_ARRAY_STR; break;
    default:
      return MMH_BAD_COORD_TYPE;
    }

  switch (code.data)
    {
    case mmh_real    : data = MMH_REAL_STR;  break;
    case mmh_complex : data = MMH_COMP_STR;  break;
    case mmh_pattern : data = MMH_PAT_STR;   break;
    case mmh_integer : data = MMH_INT_STR;   break;
    default:
      return MMH_BAD_DATA_TYPE;
    }


  switch (code.storage)
    {
    case mmh_general   : storage = MMH_GEN_STR;  break;
    case mmh_symmetric : storage = MMH_SYMM_STR; break;
    case mmh_skew      : storage = MMH_SKEW_STR; break;
    case mmh_hermitian : storage = MMH_HERM_STR; break;
    default:
      return MMH_BAD_STORAGE_TYPE;
    }

  if (fprintf(f, "%s %s %s %s %s\n",
	      banner, mtx, coord, data, storage) < 0)
    return MMH_FWRITE;

  return 0;
}
