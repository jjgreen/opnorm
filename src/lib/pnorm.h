/*
  pnorm.h

  vector p-norms

  Copyright (c) J.J. Green 2012
*/

#ifndef PNORM_H
#define PNORM_H

#include "index.h"

double pnorm(const double *v, index_t n, double p);

#endif
