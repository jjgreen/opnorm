/*
  simple timing tool for opnorm
  J.J. Green 2012
*/

#define _POSIX_C_SOURCE 199506L

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "opnorm.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* uniformly distributed in [0,1] */

static double uniform(void)
{
  return (double)rand()/RAND_MAX;
}

/* normally distributed (using box-meuller) */

static double normal(void)
{
  double
    u1 = uniform(),
    u2 = uniform();

  return sqrt(-2*log(u1))*cos(2*M_PI*u2);
}

typedef struct timespec timespec_t;

/* return time difference as a double */

static double get_walltime(timespec_t ts0, timespec_t ts1)
{
  long int ds  = ts1.tv_sec - ts0.tv_sec;
  long int dns = ts1.tv_nsec - ts0.tv_nsec;

  return ds + dns*1e-9;
}

int main(int argc, const char** argv)
{
  if (argc != 4)
    {
      fprintf(stderr, "usage %s tests order p\n", argv[0]);
      return EXIT_FAILURE;
    }

  srand(2);

  unsigned int i,
    n = atol(argv[1]),
    m = atol(argv[2]);
  double
    p = atof(argv[3]);

  opnorm_opt_t opt = {
    .eps     = 1e-14,
    .fifomax = 0
  };

  /* for cpu time */
  clock_t c0,c1;
  c0 = clock();

  /* for wall time */
  timespec_t ts0, ts1;
  clock_gettime(CLOCK_MONOTONIC, &ts0);

  unsigned long neval = 0, nfifo = 0, fifomax = 0;

  for (i=0 ; i<n ; i++)
    {
      double A[m*m], norm;
      opnorm_stats_t stats;
      unsigned int j;

      for (j=0 ; j<m*m ; j++)
	{
	  A[j] = normal();
	}

      int err = opnorm(A, column_major, m, m, p, p,
		       opt, &norm, NULL, &stats);

      if (err != 0)
	{
	  fprintf(stderr, "error : %s", opnorm_strerror(err));
	  return EXIT_FAILURE;
	}

      neval   += stats.neval;
      nfifo   += stats.nfifo;
      fifomax += stats.fifomax;
    }

  c1 = clock();
  double cpu = ((double)(c1 - c0))/(double)CLOCKS_PER_SEC;

  clock_gettime(CLOCK_MONOTONIC, &ts1);
  double wall = get_walltime(ts0,ts1);

  printf("%4.2f %2u %10.6e %10.6e %.2f %.2f %.2f\n",
	 p, m, cpu/n, wall/n,
	 (double)neval/n,
	 (double)nfifo/n,
	 (double)fifomax/n);

  return EXIT_SUCCESS;
}
