/*
  opnorm.cc
  octave C++ interface to opnorm()

  J.J. Green 2011, 2015
*/

#include <octave/oct.h>
#include <octave/oct-map.h>
#include <octave/version.h>

/* for pre-3.8 versions of octave */

#if OCTAVE_MAJOR_VERSION < 3 || \
  (OCTAVE_MAJOR_VERSION == 3 && OCTAVE_MINOR_VERSION < 8)
#define octave_scalar_map Octave_map
#endif

#include <cerrno>
#include <cstring>

#include "opnorm.h"

#define DEFAULT_EPS     1e-10
#define DEFAULT_FIFOMAX     0

static octave_scalar_map oct_stats(opnorm_stats_t stats)
{
  octave_scalar_map omap;

  omap.assign("neval",   stats.neval);
  omap.assign("nfifo",   stats.nfifo);
  omap.assign("fifomax", stats.fifomax);
  omap.assign("nthread", stats.nthread);

  return omap;
}

DEFUN_DLD (opnorm, args, nargout,
	   "-*- texinfo -*-\n"
	   "@deftypefn {Function File} "
	   "{[@var{N}, @var{v}, @var{stats}] =} "
	   "opnorm (@var{A}, @var{p}, @var{q}, @var{epsilon}, @var{fifomax})"
	   "\n\n"
	   "Return the operator norm @var{N} of the real m x n matrix @var{A} "
	   "considered as an operator"
	   "\n\n"
	   "A : (R^m, ||.||p) -> (R^n, ||.||q)"
	   "\n\n"
	   "using global optimisation on the (R^m, ||.||p) unit ball. The result "
	   "is returned to the requested accuracy @var{epsilon}. "
	   "If omitted, @var{q} will taken to be @var{p}, @var{epsilon} to be "
	   "1e-10 and @var{fifomax} to be zero (no limit on fifo size)."
	   "\n\n"
	   "The column vector @var{v} is a maximising vector and the struct "
	   "@var{stats} gives some statistics on the calculation:"
	   "\n\n"
	   "@itemize @bullet\n"
	   "@item\n"
	   "@var{neval} the number of evaluations (matrix-vector multiplications)\n"
	   "@item\n"
	   "@var{nfifo} the throughput of the fifo\n"
	   "@item\n"
	   "@var{fifomax} the maximum size of the fifo\n"
	   "@item\n"
	   "@var{nthread} the number of threads used\n"
	   "@end itemize"
	   "\n\n"
	   "The algorithm, due to S. W. Drury, is exponential in the number of "
	   "columns of the matrix @var{A} (typically 8 or 9 is the limit) but "
	   "linear in the number of rows (several thousands are possible). Note "
	   "also that the method slows as P approches one."
	   "\n\n"
	   "The parameters must satisfy 1 < @var{p} < Inf and 1 <= @var{q} <= Inf."
	   "\n\n"
	   "@seealso{norm}\n"
	   "@end deftypefn")
{
  /* output arguments */

  if (nargout > 3)
    {
      octave_stdout
	<< "only 3 outputs, the rest will not be assigned\n";
      nargout = 3;
    }

  /* input argument counts */

  int nargin = args.length();

  if (nargin < 2)
    {
      octave_stdout << "at least two arguments required\n";
      print_usage();
      return octave_value_list();
    }

  if (nargin > 5)
    {
      octave_stdout << "too many arguments\n";
      print_usage();
      return octave_value_list();
    }

  /* input argument types */

  if (not args(0).is_real_matrix())
    {
      octave_stdout << "first argument must be real matrix\n";
      return octave_value_list();
    }

  int i;

  for (i=1 ; i<5 ; i++)
    {
      if ((nargin > i) && (not args(i).is_real_scalar()))
	{
	  octave_stdout
	    << "argument "
	    << i+1
	    << " must be real scalar\n";
	  return octave_value_list();
	}
    }

  /* load input arguments */

  const Matrix A(args(0).matrix_value());

  double p, q;
  opnorm_opt_t opt;

  p = args(1).double_value();

  if (nargin >= 3)
    {
      q = args(2).double_value();
    }
  else
    {
      q = p;
    }

  if (nargin >= 4)
    {
      opt.eps = args(3).double_value();
    }
  else
    {
      opt.eps = DEFAULT_EPS;
    }

  if (nargin >= 5)
    {
      opt.fifomax = args(4).ulong_value();
    }
  else
    {
      opt.fifomax = DEFAULT_FIFOMAX;
    }

  /*
    dimensions of input matrix
  */

  dim_vector dim = A.dims();
  int m = dim(0), n = dim(1);

  /* get the raw array of doubles representing A */

  const double *Adat = A.data();

  /* maximiser */

  double vdat[n];

  /* now run opnorm */

  double N = 0;
  opnorm_stats_t stats;

  int err =
    opnorm(Adat, column_major, m, n, p, q, opt, &N, vdat, &stats);

  /* check for error */

  if (err)
    {
      octave_stdout
	<< opnorm_strerror(err)
	<< "\n";
      return octave_value_list();
    }

  /* return results */

  octave_value_list retval;

  switch (nargout)
    {
    case 3:
      retval(2) = oct_stats(stats);
    case 2:
      {
        ColumnVector vmax(n);

        for (std::size_t i = 0 ; i < n ; i++)
          vmax(i) = vdat[i];

        retval(1) = vmax;
      }
    case 1:
    case 0:
      retval(0) = N;
      break;
    default:
      octave_stdout << "strange case\n";
      break;
    }

  return retval;
}
