/*
  tests_pnorm.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_PNORM_H
#define TESTS_PNORM_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_pnorm[];

void test_pnorm_pythagorean_triples(void);
void test_pnorm_pythagorean_quadruplets(void);
void test_pnorm_faulhaber6(void);
void test_pnorm_jacobi_madden(void);
void test_pnorm_infinity(void);

#endif
