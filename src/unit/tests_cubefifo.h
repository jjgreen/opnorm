/*
  tests_cubefifo.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_CUBEFIFO_H
#define TESTS_CUBEFIFO_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cubefifo[];
void test_cubefifo_init_fifo(void);
void test_cubefifo_empty_fifo(void);

#endif
