/*
  tests_opnorm.c
  Copyright (c) J.J. Green 2014
*/

#include <float.h>

#include <opnorm.h>
#include <mmhio.h>
#include <mmdio.h>
#include <status.h>
#include "tests_opnorm.h"
#include "helper.h"

/*
  No tests that the correct values are returned,
  those are in the acceptance tests -- here we
  test the error behaviour given bad arguments etc
*/

CU_TestInfo tests_opnorm[] =
  {
    {"p/q arguments", test_opnorm_pq},
    {"epsilon", test_opnorm_eps},
    {"fifo maximum", test_opnorm_fifomax},
    {"Drury K matrix", test_opnorm_drury_k},
    {"random 7x7 matrix", test_opnorm_random_7x7},
    CU_TEST_INFO_NULL,
  };

/* traps for p/q domain errors */

void test_opnorm_pq(void)
{
  double M[4] = {1, 1, 1, 1}, norm = 0, vmax[2] = {0, 0};
  opnorm_opt_t opt = { .eps = 1e-10, .fifomax = 0 };
  opnorm_stats_t stats;
  int err;

  err = opnorm(M, row_major, 2, 2, 1, 2, opt, &norm, vmax, &stats);
  CU_ASSERT( err == OPNORM_EDOM_P );
  CU_ASSERT( norm == 0 );

  err = opnorm(M, row_major, 2, 2, INFINITY, 2, opt, &norm, vmax, &stats);
  CU_ASSERT( err == OPNORM_EDOM_P );
  CU_ASSERT( norm == 0 );

  err = opnorm(M, row_major, 2, 2, 2, 0.99, opt, &norm, vmax, &stats);
  CU_ASSERT( err == OPNORM_EDOM_Q );
  CU_ASSERT( norm == 0 );
}

/* traps for infeasible epsilon values */

void test_opnorm_eps(void)
{
  double M[4] = {1, 1, 1, 1}, norm = 0, vmax[2] = {0, 0};
  opnorm_opt_t opt = { .fifomax = 0 };
  opnorm_stats_t stats;
  int err;

  opt.eps = -1;
  err = opnorm(M, row_major, 2, 2, 4, 4, opt, &norm, vmax, &stats);
  CU_ASSERT( err == OPNORM_EDOM_EPS );
  CU_ASSERT( norm == 0 );

  opt.eps = DBL_EPSILON/2;
  err = opnorm(M, row_major, 2, 2, 4, 4, opt, &norm, vmax, &stats);
  CU_ASSERT( err == OPNORM_INACC );
  CU_ASSERT( norm == 0 );
}

/*
  test of fifomax option
*/

void test_opnorm_fifomax(void)
{
  double M[4] = {1, 1, 1, 1}, norm = 0, vmax[2] = {0, 0};
  opnorm_opt_t opt = { .eps = 1e-14, .fifomax = 3 };
  opnorm_stats_t stats = { .fifomax = 12345 };
  int err;

  err = opnorm(M, row_major, 2, 2, 4, 4, opt, &norm, vmax, &stats);
  CU_ASSERT( err == OPNORM_FIFO );
  CU_ASSERT( stats.fifomax == 12345 );
  CU_ASSERT( norm == 0.0 );
}

/*
  test of the Drury K matrix
*/

/* portmanteau matrix struct for mmdio */

typedef struct
{
  size_t m, n;
  double *v;
} matrix_t;

/*
  callback for mmdio, assign the (i,j)th entry of the
  matrix M
*/

static int mmdr_cb(size_t i,size_t j,double v, matrix_t *M)
{
  size_t ncol = M->n;

  M->v[i*ncol + j] = v;

  return 0;
}

static void test_opnorm_mtx(const char *path, double p, double q, double true_norm)
{
  FILE* st = fopen(path, "r");

  CU_TEST_FATAL( st != NULL );

  mmh_code_t code;

  CU_TEST_FATAL( mmh_read_banner(st, &code) == 0 );

  matrix_t M;

  CU_TEST_FATAL( code.coord == mmh_coordinate );

  size_t nnz;

  CU_TEST_FATAL( mmh_read_mtx_crd_size(st, &(M.m), &(M.n), &nnz) == 0 );
  CU_TEST_FATAL( code.data == mmh_real );

  M.v = calloc(M.m * M.n, sizeof(double));

  CU_TEST_FATAL( M.v != NULL );

  CU_TEST_FATAL( mmdr_cr(st, nnz, (mmdr_cbr_t)mmdr_cb, &M) == 0 );

  opnorm_opt_t opt;
  opnorm_stats_t stats;
  double norm;
  double vmax[M.n];

  opt.eps = 1e-10;
  opt.fifomax = 0;

  int err = opnorm(M.v, row_major, M.m, M.n, p, q, opt,
                   &norm, vmax, &stats);

  CU_ASSERT_EQUAL(err, OPNORM_OK);
  CU_ASSERT_DOUBLE_EQUAL(norm, true_norm, opt.eps * true_norm);
}

void test_opnorm_drury_k(void)
{
  test_opnorm_mtx("../mtx/drury/K.mtx", 4, 4, 0.99999943874177);
}

void test_opnorm_random_7x7(void)
{
  test_opnorm_mtx("../mtx/random/R07x07.mtx", 4, 4, 1.721052943948);
}
