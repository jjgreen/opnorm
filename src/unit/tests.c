/*
  tests.c
  testcase loader
  J.J.Green 2014
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "tests_cube.h"
#include "tests_cubefifo.h"
#include "tests_matvec.h"
#include "tests_opnorm.h"
#include "tests_patch.h"
#include "tests_pnorm.h"
#include "tests_status.h"

#include "cunit_compat.h"

#define ENTRY(a, b) CU_Suite_Entry((a), NULL, NULL, (b))

#include <CUnit/CUnit.h>

static CU_SuiteInfo suites[] =
  {
    ENTRY("cube", tests_cube),
    ENTRY("cubefifo", tests_cubefifo),
    ENTRY("matvec", tests_matvec),
    ENTRY("opnorm", tests_opnorm),
    ENTRY("patch", tests_patch),
    ENTRY("pnorm", tests_pnorm),
    ENTRY("status", tests_status),
    CU_SUITE_INFO_NULL,
  };

void tests_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr, "suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
