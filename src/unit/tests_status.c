/*
  tests_status.c
  Copyright (c) J.J. Green 2015
*/

#include <opnorm.h>
#include <status.h>
#include "tests_status.h"
#include "helper.h"

CU_TestInfo tests_status[] =
  {
    {"valid errors",  test_status_valid},
    {"unknown error", test_status_unknown},
    CU_TEST_INFO_NULL,
  };

typedef struct {
  int err;
  const char *msg;
} mtab_t;

void test_status_valid(void)
{
  mtab_t mtab[] =
    {
      { OPNORM_OK,      "Success" },
      { OPNORM_EDOM_P,  "Domain error (1 < p < infinity required)" },
      { OPNORM_EDOM_Q,  "Domain error (1 <= q required)" },
      { OPNORM_EDOM_EPS,"Domain error (0 < eps required)" },
      { OPNORM_INACC,   "Requested accuracy cannot be achieved" },
      { OPNORM_FIFO,    "Error in FIFO" },
      { OPNORM_ALLOC,   "Error allocating memory" },
      { OPNORM_USER,    "User interruption" },
      { OPNORM_THREAD,  "Thread error" },
      { OPNORM_BUG,     "Bug tripped, please report" },
      { OPNORM_FIFOMAX, "User specifed FIFO max-length exceeded" }
    };
  size_t n = sizeof(mtab) / sizeof(mtab_t);

  for (size_t i = 0 ; i < n ; i++)
    {
      const char *msg = opnorm_strerror(mtab[i].err);

      CU_ASSERT(msg != NULL);
      CU_ASSERT_STRING_EQUAL(msg, mtab[i].msg);
    }
}

void test_status_unknown(void)
{
  const char* msg = opnorm_strerror(12345);

  CU_ASSERT(msg != NULL);
  CU_ASSERT_STRING_EQUAL(msg, "unimplemented message");
}
