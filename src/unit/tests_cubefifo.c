/*
  tests_cubefifo.c
  Copyright (c) J.J. Green 2014
*/

#include <float.h>

#include <cubefifo.h>
#include "tests_cubefifo.h"

CU_TestInfo tests_cubefifo[] =
  {
    {"initialise", test_cubefifo_init_fifo},
    {"empty",      test_cubefifo_empty_fifo},
    CU_TEST_INFO_NULL,
  };

void test_cubefifo_init_fifo(void)
{
  fifo_t *fifo;

  fifo = init_fifo(5, 0);
  CU_ASSERT(fifo != NULL);
  fifo_destroy(fifo);
}

void test_cubefifo_empty_fifo(void)
{
  fifo_t *fifo;

  fifo = init_fifo(5, 0);
  CU_ASSERT(empty_fifo(fifo) == 0);
  fifo_destroy(fifo);
}
