/*
  tests_status.h
  Copyright (c) J.J. Green 2015
*/

#ifndef TESTS_STATUS_H
#define TESTS_STATUS_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_status[];
void test_status_valid(void);
void test_status_unknown(void);

#endif
