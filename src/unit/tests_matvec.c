/*
  tests_matvec.c
  Copyright (c) J.J. Green 2014
*/

#include <float.h>

#include <matvec.h>
#include "tests_matvec.h"
#include "helper.h"

CU_TestInfo tests_matvec[] =
  {
    {"column major", test_matvec_colmajor},
    {"row major",    test_matvec_rowmajor},
    CU_TEST_INFO_NULL,
  };

#define TEST_EPS (1.5 * DBL_EPSILON)

void test_matvec_colmajor(void)
{
  double A[4] = {1, 2, 3, 4};
  double x[2] = {-1, 1}, y[2];

  matvec_column_major(A, x, 2, 2, y);

  CU_ASSERT( eq_rel(2.0, y[0], TEST_EPS) );
  CU_ASSERT( eq_rel(2.0, y[1], TEST_EPS) );
}

void test_matvec_rowmajor(void)
{
  double A[4] = {1, 2, 3, 4};
  double x[2] = {-1, 1}, y[2];

  matvec_row_major(A, x, 2, 2, y);

  CU_ASSERT( eq_rel(1.0, y[0], TEST_EPS) );
  CU_ASSERT( eq_rel(1.0, y[1], TEST_EPS) );
}
