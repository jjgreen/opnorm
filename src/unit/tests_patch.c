/*
  tests_patch.c
  Copyright (c) J.J. Green 2014
*/

#include <float.h>

#include <patch.h>
#include <lcrp.h>
#include <cube.h>
#include <pnorm.h>

#include "tests_patch.h"
#include "helper.h"

CU_TestInfo tests_patch[] =
  {
    {"corner 2D", test_patch_corner_2d},
    CU_TEST_INFO_NULL,
  };

#define TEST_EPS (1.5 * DBL_EPSILON)

/*
  This rather complicated test checks that the projection
  of the corners of a boundary cube (i.e., the two points
  at the ends of a line segment) are at a distance smaller
  than the 'patch radius' from the 'patch centres'
*/

static void project_onto_p_ball(double *v, double p, double *w)
{
  double nv = pnorm(v, 2, p);
  int i;

  for (i=0 ; i<2 ; i++)
    w[i] = v[i]/nv;
}

static double vector_dist(double *a, double *b)
{
  double c[2] = {a[0] - b[0], a[1] - b[1]};
  return pnorm(c, 2, 2);
}

void test_patch_corner_2d(void)
{
  double ps[5] = { 1.0, 1.5, 4.0, 10.0, INFINITY };
  int i;
  double cube_centres[2] = {1.0, 0.5};
  cube_t cube = {
    .side    = 0,
    .hwnexp  = 4,
    .centres = cube_centres
  };
  double hw = cube_halfwidth(&cube);
  double
    v1[2] = {cube.centres[0], cube.centres[1] + hw},
    v2[2] = {cube.centres[0], cube.centres[1] - hw};

  for (i=0 ; i<5 ; i++)
    {
      double L, p = ps[i], w1[2], w2[2];
      double S = pow(2-1, 1/p);
      double patch_centres[2];
      patch_t patch = { .centres = patch_centres };

      CU_ASSERT( lcrp(p, &L) == LCRP_OK );
      cube_to_patch(&cube, 2, p, L, S, &patch);

      project_onto_p_ball(v1, p, w1);
      project_onto_p_ball(v2, p, w2);

      CU_ASSERT( vector_dist(patch.centres, w1) < patch.radius );
      CU_ASSERT( vector_dist(patch.centres, w2) < patch.radius );
    }
}
