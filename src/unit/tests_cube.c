/*
  tests_cube.c
  Copyright (c) J.J. Green 2014
*/

#include <float.h>

#include <cube.h>
#include "tests_cube.h"

CU_TestInfo tests_cube[] =
  {
    {"half-width", test_cube_halfwidth},
    CU_TEST_INFO_NULL,
  };

void test_cube_halfwidth(void)
{
  cube_t cube = {
    .side   = 1,
    .hwnexp = 3,
    .centres = NULL
  };

  double hw = cube_halfwidth(&cube);

  CU_ASSERT_DOUBLE_EQUAL(hw, 0.125, DBL_EPSILON);
}
