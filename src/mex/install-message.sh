#! /bin/sh
OBJ=$1
cat <<EOF
---------------------------------------------------------
There is not really a sensible default location to 
install Matlab mex files, so you will need to install

  src/mex/opnorm.m
  src/mex/$OBJ

somewhere on your Matlab path by hand.  

To determine (or alter) the Matlab path run the command 

  path 

on the Matlab prompt, or visit the Matlab online 
documentation at

  http://www.mathworks.co.uk/help/matlab/search-path.html

---------------------------------------------------------
EOF