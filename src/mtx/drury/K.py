#! /usr/bin/python
#
# generate the matrix K of S. Drury

import numpy as np
import scipy.io.mmio as sim
import scipy.sparse as ss

C  = 6.52773
c  = (1.0, 2.0, -4.4);
mu = (0.263485,
      0.385467,
      0.360598,
      0.302274,
      0.165718)
nu = (0.064388,
      0.146465,
      0.300904,
      0.364746,
      0.318525,
      0.247617,
      0.123827)

M = np.zeros([7, 5])

for i in range(7) :
    for j in range(5) :
        if j <= i <= j+2 :
            M[i,j] = mu[j]*c[i-j]/(nu[i] * C)

sim.mmwrite("K.mtx", ss.lil_matrix(M))
