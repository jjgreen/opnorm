# read the matrix K of S. Drury and compare its (4, 4)-operator
# norm with the value found in Drury's implementation

from opnorm import opnorm
import pytest


@pytest.fixture
def K():
    import scipy.io as sio
    from pathlib import Path
    relpath = Path('../../mtx/drury/K.mtx')
    path = Path(__file__).resolve().parent / relpath
    return sio.mmread(str(path)).todense()


def test_norm(K):
    '''
    correct to specified accuracy
    '''
    M0 = 0.99999943874177
    for eps in (1e-10, 1e-8, 1e-6, 1e-4):
        (M, _, _) = opnorm(K, 4, 4, eps=eps)
        assert abs(M - M0) < eps * M


def test_has_stats(K):
    '''
    returns a stats dict with expected keys
    '''
    (_, _, stats) = opnorm(K, 4, 4, eps=1e-10, fifomax=0)
    keys = ['neval', 'nfifo', 'fifomax', 'nthread']
    assert set(stats.keys()) == set(keys)


def test_fifomax(K):
    '''
    over-small fifomax raises RuntimError
    '''
    with pytest.raises(RuntimeError) as err:
        opnorm(K, 4, 4, eps=1e-10, fifomax=10)
    assert str(err.value) == 'User specifed FIFO max-length exceeded'


def test_pq_required(K):
    '''
    not specifying p, q raises ValueError
    '''
    with pytest.raises(ValueError) as err:
        opnorm(K)
    assert str(err.value) == 'error parsing arguments'


def test_q_domain(K):
    '''
    out-of range q raises ValueError
    '''
    with pytest.raises(ValueError) as err:
        opnorm(K, p=2, q=0.5)
    assert str(err.value) == 'Domain error (1 <= q required)'


def test_p_domain(K):
    '''
    out-of range p raises ValueError
    '''
    with pytest.raises(ValueError) as err:
        opnorm(K, p=0.5, q=2)
    assert str(err.value) == 'Domain error (1 < p < infinity required)'
