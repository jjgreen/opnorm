function accept_hilbert()
% these tests use the fact that matlab/octave can
% easily find the (2,2)-operator norm of a matrix,
% it is the square root of the largest singular
% value

    rand('seed', 42);
    epsilon = 1e-10;

    for i = 2:5
        for j = 1:10
            A = rand(i) - 0.5;
            N1 = norm(A, 2);
            N2 = opnorm(A, 2, 2, epsilon);
            assert(abs(N1 - N2) / N2 < epsilon, '2-norm');
        end
    end