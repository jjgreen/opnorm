function accept_drakakis()
% these tests compare opnorm against the algorithm
% of Drakakis for the (2,1)-operator norm of a matrix
% (implemented in opnorm21.m)

   rand('seed', 42);
   epsilon = 1e-10;
   for i = 2:7
       for j = 1:10
           A = rand(i) - 0.5;
           N1 = opnorm21(A);
           N2 = opnorm(A, 2, 1, epsilon);
           assert(abs(N1 - N2) / N2 < epsilon, 'drakakis')
       end
   end