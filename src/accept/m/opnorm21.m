function M = opnorm21(A)
% OPNORM21
%
% Implementation of the l2 -> l1 operator norm of Drakakis
% described in [1], this is clearly exponential in the
% number of rows of the matrix A
%
% [1] K. Drakakis, "On the calculation of the l2 -> l1
% induced matrix norm", Int. J. Algebra, 2009 (3), No 5,
% 231-240

    [nr, nc] = size(A);

    v  = [1 -1];

    for i = 1:nr-1
        v = [repmat(v, [1 2])
            ones(1, 2^i) -ones(1, 2^i)];
    end

    u = A' * v;

    M = sqrt(max(sum(u.^2, 1)));

end
