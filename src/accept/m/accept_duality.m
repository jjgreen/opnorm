function accept_duality()
% this test checks duality, that
%
%    opnorm(A, p, q) == opnorm(A', q', p')
%
% to accuracy 2 epsilon, where A' is the transpose
% of A, p' is the conjugate of p and q' is the conjugate
% of q.  Because the algorithm slows dramatically as
% p (or p') approaches 1 we limit p,q to a few values
% between 3/2 and 2, and use only a single randomly
% chosen 3x3 matrix

    epsilon = 1e-9;
    rand('seed', 42);
    A = rand(3) - 0.5;

    for i = 0:9
        p = 1.5 + i * 0.05;
        pc = 1 / (1 - 1/p);
        for j = 0:9
            q = 1.5 + j * 0.05;
            qc = 1 / (1 - 1/q);
            N1 = opnorm(A, p, q, epsilon);
            N2 = opnorm(A', qc, pc, epsilon);
            assert(abs(N1 - N2) < 2 * epsilon, 'duality');
        end
    end
